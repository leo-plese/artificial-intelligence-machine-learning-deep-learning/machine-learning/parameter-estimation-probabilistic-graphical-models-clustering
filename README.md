Parameter Estimation, Probabilistic Graphical Models and Clustering. Tasks:

1 Maximum Likelihood Estimation (MLE) and Maximum A Posteriori Estimation (MAP)

2 Probabilistic graphical models - Bayesian networks

3 Clustering (Gaussian Mixture Models - GMM, Rand index)

Implemented in Python using NumPy, scikit-learn, Matplotlib and pgmpy library (for probabilistic graphical models). Code and task description in the Jupyter Notebook.

My lab assignment in Machine Learning, FER, Zagreb.

Created: 2021
